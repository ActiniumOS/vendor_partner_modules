//
// Copyright (C) 2021 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package {
    default_applicable_licenses: [
        "Android-Apache-2.0",
        "vendor_unbundled_google_modules_CellBroadcastGooglePrebuilt_license",
    ],
}

license {
    name: "vendor_unbundled_google_modules_CellBroadcastGooglePrebuilt_license",
    license_kinds: [
        "SPDX-license-identifier-Apache-2.0",
    ],
    license_text: ["LICENSE"],
}

soong_config_module_type_import {
    from: "packages/modules/common/Android.bp",
    module_types: ["module_apex_set"],
}

module_apex_set {
    name: "com.google.android.cellbroadcast",
    apex_name: "com.android.cellbroadcast",
    owner: "google",
    overrides: ["com.android.cellbroadcast"],
    filename: "com.google.android.cellbroadcast.apex",
    set: "com.google.android.cellbroadcast.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    required: [
        "GoogleCellBroadcast_config.xml",
    ],
}

module_apex_set {
    name: "com.google.android.cellbroadcast_compressed",
    apex_name: "com.android.cellbroadcast",
    owner: "google",
    overrides: ["com.android.cellbroadcast"],
//    filename: "com.google.android.cellbroadcast.capex",
    set: "com.google.android.cellbroadcast_compressed.apks",
    prefer: true,
    soong_config_variables: {
        module_build_from_source: {
            prefer: false,
        },
    },
    required: [
        "GoogleCellBroadcast_config.xml",
    ],
}

prebuilt_etc {
    name: "GoogleCellBroadcast_config.xml",
    src: "GoogleCellBroadcast_config.xml",
    product_specific: true,
    sub_dir: "sysconfig",
}
